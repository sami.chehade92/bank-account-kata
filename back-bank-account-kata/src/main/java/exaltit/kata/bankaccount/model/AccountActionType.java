package exaltit.kata.bankaccount.model;

public enum AccountActionType {
    DEPOSIT, WITHDRAWAL, ACCOUNT_CREATION;
}
