import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../services/auth.service';
import {map, flatMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.auth.loggedIn) {
      return true;
    }

    const redirectUri = `${location.origin}/home-account`;
    const code = next.queryParams['code'];

    if (code == undefined) {
      this.router.navigate(['/login']);
      return false;
    }

    const tokenDataObs: Observable<boolean> = this.auth.retrieveTokenData(code, redirectUri)
      .pipe(
        flatMap(tokenData => {
          this.auth.logUser(tokenData);
          return this.auth.login();
        }),
        map(email => {
          console.log(email);
          return (email !== undefined);
        })
      );
    return tokenDataObs;
  }
}
