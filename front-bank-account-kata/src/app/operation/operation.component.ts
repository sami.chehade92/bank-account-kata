import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  amount: number = 0;
  statusMessageTypeEnum = StatusMessageTypeEnum;
  statusMessage = { type: this.statusMessageTypeEnum.OK, msg: '' };
  type: string | null = '';

  constructor(private accountService: AccountService, private activatedroute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedroute.paramMap.subscribe(params => {
      this.type = params.get('type');
    });
  }

  deposit() {
    this.operation(OperationTypeEnum.DEPOSIT);
  }

  withdrawal() {
    this.operation(OperationTypeEnum.WITHDRAWAL);
  }

  operation(operationType: OperationTypeEnum) {
    if (this.amount) {
      if (this.amount < 0 || this.amount > 5000) {
        this.statusMessage.type = this.statusMessageTypeEnum.ERROR;
        this.statusMessage.msg = 'Incorrect amount (must be between 0 and 5000)';
      } else {
        var operationObs;
        if (operationType == OperationTypeEnum.DEPOSIT) {
          operationObs = this.accountService.deposit(this.amount);
        } else {
          operationObs = this.accountService.withdrawal(this.amount);
        }
        if (operationObs) {
          operationObs
            .subscribe(res => {
              console.log(res);
              this.statusMessage.type = this.statusMessageTypeEnum.OK;
              this.statusMessage.msg = `${operationType} OK. New balance = ${res}`;
            });
        }
      }
    } else {
      this.statusMessage.type = this.statusMessageTypeEnum.ERROR;
      this.statusMessage.msg = 'Incorrect amount';
    }
  }
}

enum StatusMessageTypeEnum {
  OK, ERROR
}

enum OperationTypeEnum {
  DEPOSIT = 'deposit',
  WITHDRAWAL = 'withdrawal'
}
