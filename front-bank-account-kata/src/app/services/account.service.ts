import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class AccountService {

    constructor(private http: HttpClient, private authService: AuthService) { }

    deposit(amount: number): Observable<any> {
        return this.http.post(environment.server.url + '/account/deposit', amount, this.authService.tokenHeader);
    }

    withdrawal(amount: number): Observable<any> {
        return this.http.post(environment.server.url + '/account/withdrawal', amount, this.authService.tokenHeader);
    }

    retrieveHistory(): Observable<any> {
        return this.http.get(environment.server.url + '/account/history', this.authService.tokenHeader);
    }
}
