package exaltit.kata.bankaccount.controllers;

import exaltit.kata.bankaccount.model.AccountAction;
import exaltit.kata.bankaccount.model.User;
import exaltit.kata.bankaccount.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@AllArgsConstructor
public class AccountController {

    private UserService userService;

    @PostMapping("/deposit")
    private ResponseEntity<Double> deposit(@RequestBody Double amount, BearerTokenAuthentication principal) {
        User user = userService.deposit(principal, amount);
        return ResponseEntity.ok(user.getAccount().getBalance());
    }

    @PostMapping("/withdrawal")
    private ResponseEntity<Double> withdrawal(@RequestBody Double amount, BearerTokenAuthentication principal) {
        User user = userService.withdrawal(principal, amount);
        return ResponseEntity.ok(user.getAccount().getBalance());
    }

    @GetMapping("/history")
    private ResponseEntity<List<AccountAction>> history(BearerTokenAuthentication principal) {
        User user = userService.findByEmail(principal).orElseThrow();
        return ResponseEntity.ok(user.getAccount().getHistory());
    }
}
