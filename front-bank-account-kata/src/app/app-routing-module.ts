import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { OperationComponent } from './operation/operation.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeAccountComponent } from './home-account/home-account.component';
import {LoginComponent} from './login/login.component';
import { HistoryComponent } from './history/history.component';

const routes: Routes = [
  { path: 'home-account', component: HomeAccountComponent, canActivate: [AuthGuard],
    children: [
      { path: 'operation/:type', component: OperationComponent },
      { path: 'history', component: HistoryComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home-account', pathMatch: 'full' }
];

@NgModule({
  providers: [ AuthGuard ],
  imports: [ RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
