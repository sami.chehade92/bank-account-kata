import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  openidTokenUrl = environment.openid.url + environment.openid.realmUrlPrefix + environment.openid.realm +
    environment.openid.realmUrlSuffix + environment.openid.tokenEndpoint;

  retrieveTokenData(code: string, redirectUri: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + environment.openid.basicAuth
      })
    };
    const body = new HttpParams()
      .set('grant_type', 'authorization_code')
      .set('code', code)
      .set('redirect_uri', redirectUri);
    return this.http.post(this.openidTokenUrl, body, httpOptions);
  }


  logUser(datas: any) {
    localStorage.setItem('token', datas.access_token);
    localStorage.setItem('refresh_token', datas.refresh_token);
    localStorage.setItem('expires', (Date.now() + parseInt(datas.expires_in, 10) * 1000).toString());
  }

  login(): Observable<string> {
    return this.http.post<string>(environment.server.url + '/user/login', null, this.tokenHeader);
  }

  get tokenHeader() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
    };
    return httpOptions;
  }

  logout() {
    localStorage.removeItem('token');
  }

  public get loggedIn(): boolean {
    const expires = localStorage.getItem('expires');
    if (expires != null) {
      return (localStorage.getItem('token') !== null) && (Date.now() < parseInt(expires, 10));
    } else {
      return false;
    }
  }
}
