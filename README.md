# Bank Account Kata - by Sami Chehade

## Necessary tools

We list here the tools that you need to run this project. Between parentheses is the version that we personaly use to run it.

- Docker (20.10.11)
- Docker-compose (1.25.0)
- Java (11.0.13)
- Maven (3.6.3)
- Node (14.15.5)
- Npm (6.14.11)

## Run the docker images

This step runs the following docker images :
- Keycloak
- MariaDB

To run it please go to the root directory of the project and make the following commands :

```
cd bin
docker-compose -p bak up -d
```

## Run the Spring Boot project (backend)

Tu run the backend project, run the following commands from the root directory of the project :

```
cd back-bank-account-kata
mvn spring-boot:run
```

## Run the Angular project (frontend)

First, place yourself in the front project :
```
cd front-bank-account-kata
```

You should then install the needed modules :
```
npm install
```

You can then run the project :
```
ng serve
```

## Test the application

Open your favorite browser and go to this url : http://localhost:4200.

Click then on " Login with Keycloak ".

Once redirected to the login screen, connect with the following credentials :
- username : bak-user
- password : test

You're then allowed to make the different operations : Deposit, make a withdrawal, see the history (url : http://localhost:4200/home-account).
