package exaltit.kata.bankaccount.services;

import exaltit.kata.bankaccount.model.AccountAction;
import exaltit.kata.bankaccount.model.AccountActionType;
import exaltit.kata.bankaccount.model.User;
import exaltit.kata.bankaccount.repos.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;

    public User login(BearerTokenAuthentication principal) {
        String email = principal.getTokenAttributes().get("email").toString();
        return userRepository.findByEmail(email).orElseGet(() -> {
            String firstName = principal.getTokenAttributes().get("given_name").toString();
            String lastName = principal.getTokenAttributes().get("family_name").toString();
            User user = new User(firstName, lastName, email);
            userRepository.save(user);
            return user;
        });
    }

    public User deposit(BearerTokenAuthentication principal, Double amount) {
        String email = principal.getTokenAttributes().get("email").toString();
        User user = userRepository.findByEmail(email).orElseThrow();
        user.getAccount().setBalance(user.getAccount().getBalance() + amount);
        user.getAccount().getHistory().add(new AccountAction(AccountActionType.DEPOSIT, amount));
        userRepository.save(user);
        return user;
    }

    public User withdrawal(BearerTokenAuthentication principal, Double amount) {
        String email = principal.getTokenAttributes().get("email").toString();
        User user = userRepository.findByEmail(email).orElseThrow();
        user.getAccount().setBalance(user.getAccount().getBalance() - amount);
        user.getAccount().getHistory().add(new AccountAction(AccountActionType.WITHDRAWAL, amount));
        userRepository.save(user);
        return user;
    }

    public Optional<User> findByEmail(BearerTokenAuthentication principal) {
        String email = principal.getTokenAttributes().get("email").toString();
        return userRepository.findByEmail(email);
    }
}
