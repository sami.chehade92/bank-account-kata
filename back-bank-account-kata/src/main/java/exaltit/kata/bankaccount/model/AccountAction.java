package exaltit.kata.bankaccount.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class AccountAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.PRIVATE)
    private Long id;
    private AccountActionType type;
    private double amount;
    private LocalDateTime actionDateTime = Instant.now().atZone(ZoneOffset.UTC).toLocalDateTime();

    public AccountAction(AccountActionType type, double amount) {
        this.type = type;
        this.amount = amount;
    }
}
