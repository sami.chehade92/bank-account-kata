package exaltit.kata.bankaccount.controllers;

import exaltit.kata.bankaccount.model.User;
import exaltit.kata.bankaccount.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<String> login(BearerTokenAuthentication principal) {
        User user = userService.login(principal);
        return ResponseEntity.ok("\"" + user.getEmail() + "\"");
    }
}
