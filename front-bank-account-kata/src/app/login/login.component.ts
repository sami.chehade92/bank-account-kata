import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  private get openidAuthUrl(): string {
    return environment.openid.url + environment.openid.realmUrlPrefix + environment.openid.realm +
      environment.openid.realmUrlSuffix + environment.openid.authEndpoint;
  }

  private get redirectLoginUrl(): string {
    return this.openidAuthUrl +
      '?response_type=code' +
      '&scope=openid' +
      '&client_id=' + environment.openid.clientId +
      '&redirect_uri=' + location.origin + '/home-account';
  }

  redirectLogin() {
    window.location.href = this.redirectLoginUrl;
    return false;
  }
}
