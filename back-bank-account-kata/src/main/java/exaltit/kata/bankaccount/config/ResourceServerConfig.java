package exaltit.kata.bankaccount.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {

    @Value("${oidc.introspectionUri}")
    private String introspectionUri;
    @Value("${oidc.clientId}")
    private String clientId;
    @Value("${oidc.clientSecret}")
    private String clientSecret;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .oauth2ResourceServer(
                        c -> c.opaqueToken(
                                o -> {
                                    o.introspectionUri(introspectionUri);
                                    o.introspectionClientCredentials(clientId, clientSecret);
                                }
                        )
                );
    }
}
