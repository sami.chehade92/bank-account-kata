package exaltit.kata.bankaccount.services;

import exaltit.kata.bankaccount.model.AccountActionType;
import exaltit.kata.bankaccount.model.User;
import exaltit.kata.bankaccount.repos.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;
    @Mock
    private UserRepository userRepository;

    @Test
    public void login_newUser() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.empty());
        User user = userService.login(bearerTokenAuthentication);
        assertAll(
                () -> assertEquals("myemail", user.getEmail()),
                () -> assertEquals("Sami", user.getFirstName()),
                () -> assertEquals("Chehade", user.getLastName()),
                () -> assertEquals(0, user.getAccount().getBalance()),
                () -> assertEquals(1, user.getAccount().getHistory().size()),
                () -> assertEquals(AccountActionType.ACCOUNT_CREATION, user.getAccount().getHistory().get(0).getType())
        );
    }

    @Test
    public void login_existingUser() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        User user = new User("Maxime", "Delfort", "myemail");
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.of(user));
        User obtainedUser = userService.login(bearerTokenAuthentication);
        assertEquals("myemail", obtainedUser.getEmail());
        assertEquals("Maxime", obtainedUser.getFirstName());
        assertEquals("Delfort", obtainedUser.getLastName());
    }

    @Test
    public void deposit() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        User user = new User("Sami", "Chehade", "myemail");
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.of(user));
        User obtainedUser = userService.deposit(bearerTokenAuthentication, 12.4);
        assertAll(
                () -> assertEquals(12.4, obtainedUser.getAccount().getBalance()),
                () -> assertEquals(AccountActionType.DEPOSIT, obtainedUser.getAccount().getHistory().get(1).getType()),
                () -> assertEquals(12.4, obtainedUser.getAccount().getHistory().get(1).getAmount())
        );
    }

    @Test
    public void deposit_userDoesntExist() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class, () -> userService.deposit(bearerTokenAuthentication, 12.4));
    }

    @Test
    public void withdrawal() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        User user = new User("Sami", "Chehade", "myemail");
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.of(user));
        User obtainedUser = userService.withdrawal(bearerTokenAuthentication, 12.4);
        assertAll(
                () -> assertEquals(-12.4, obtainedUser.getAccount().getBalance()),
                () -> assertEquals(AccountActionType.WITHDRAWAL, obtainedUser.getAccount().getHistory().get(1).getType()),
                () -> assertEquals(12.4, obtainedUser.getAccount().getHistory().get(1).getAmount())
        );
    }

    @Test
    public void withdrawal_userDoesntExist() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.empty());
        assertThrows(NoSuchElementException.class, () -> userService.withdrawal(bearerTokenAuthentication, 12.4));
    }

    @Test
    public void findByEmail() {
        var bearerTokenAuthentication = getBearerTokenAuthentication();
        User user = new User("Sami", "Chehade", "myemail");
        when(userRepository.findByEmail("myemail")).thenReturn(Optional.of(user));
        User obtainedUser = userService.findByEmail(bearerTokenAuthentication).get();
        assertEquals(user, obtainedUser);
    }

    public BearerTokenAuthentication getBearerTokenAuthentication() {
        OAuth2AuthenticatedPrincipal oAuth2AuthenticatedPrincipal = new OAuth2IntrospectionAuthenticatedPrincipal(
                Map.of(
                        "email", "myemail",
                        "given_name", "Sami",
                        "family_name", "Chehade"
                ), null);
        OAuth2AccessToken oAuth2AccessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, "tokenvalue",
                Instant.now(), Instant.now());
        return new BearerTokenAuthentication(oAuth2AuthenticatedPrincipal, oAuth2AccessToken, null);
    }
}
