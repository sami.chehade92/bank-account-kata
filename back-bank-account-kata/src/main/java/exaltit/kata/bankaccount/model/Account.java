package exaltit.kata.bankaccount.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Setter
    private double balance;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    private List<AccountAction> history;

    public Account() {
        AccountAction accountAction = new AccountAction();
        accountAction.setType(AccountActionType.ACCOUNT_CREATION);
        this.history = new ArrayList<>(List.of(accountAction));
    }
}
