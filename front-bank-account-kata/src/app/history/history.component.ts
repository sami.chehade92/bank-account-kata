import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  history: Array<any> = [];

  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.accountService.retrieveHistory()
      .subscribe(res => {
        console.log(res);
        this.history = res.reverse();
      });
  }

  get historyLength() {
    return this.history.length;
  }

  displayDate(date: any) {
    return `${date[2]}/${date[1]}/${date[0]}, ${date[3]}:${date[4]}`;
  }
}
